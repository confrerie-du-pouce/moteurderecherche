# Moteur De Recherche

Ce projet a pour but de représenté un moteur de recherche avec différents algorithme de recheche sur une base de donnée déjà établie.

## Fonctionnalités

- Cocher les différentes categories désirées puis et choisissez parmis :
    - Recherche par cosine similarity
    - Recherche par Path Length similarity
    - Recherche par Semantic similarity
- Les resultats obtenues sont les 10 articles avec les similarité les plus élevées

## Installation et lancement

- lancer le serveur sur windows : `set DEBUG=myapp:* & npm start`
- lancer le serveur sur macOS ou Linux : `DEBUG=myapp:* & npm start`
- le site se trouve à l'addresse : `http://localhost:3000`

