const fs = require('fs');

module.exports = class SearchEngine{
    selectedItems;
    wordnetList;
    typeid;
    vector;
    articlevectors;
    articletypeid;
    articlenames;
    nbarticle;
    treeData;
    allFreq;
    constructor(selectedItems) {
        this.selectedItems = Object.keys(selectedItems);
        let wordnets = fs.readFileSync("public/entities/dimension_name.txt");
        this.wordnetList = wordnets.toString().split(/\r?\n/);
        let typdeid = fs.readFileSync("public/entities/typeid_name_wordnet.json");
        this.typeid = JSON.parse(typdeid);
        let vectors = fs.readFileSync("public/entities/article_vector.json");
        this.articlevectors = JSON.parse(vectors);
        let vectors2 = fs.readFileSync("public/entities/article_typeid.json");
        this.articletypeid = JSON.parse(vectors2);
        this.nbarticle = Object.keys(this.articlevectors).length;
        this.articlenames = Object.keys(this.articlevectors);
        this.vector = [];
        this.allFreq = [];
        for(let i = 0; i<this.wordnetList.length; i++){
            this.vector.push(0);
        }
        this.createVector();
    }

    createVector(){
        for(let j = 0; j<this.selectedItems.length; j++){
            if(this.selectedItems[j] == 'entity'){
                continue;
            }
            for(let k = 0; k<this.wordnetList.length; k++){
                if(this.typeid[this.selectedItems[j]]["type"] == this.wordnetList[k]){
                    this.vector[k] += 1;
                }
            }
        }
    }

    cosineComputing(){
        let articleSimilarity = [];
        for(let i = 0; i < this.articlenames.length; i++){
            let upper = 0;
            let lowerfirst = 0;
            let lowersecond = 0;
            for(let j = 0; j < this.wordnetList.length; j++){
                upper += (this.vector[j] * this.articlevectors[this.articlenames[i]][j]);
                lowerfirst += Math.pow(this.vector[j],2);
                lowersecond += Math.pow(this.articlevectors[this.articlenames[i]][j], 2);
            }
            let lower = Math.sqrt(lowerfirst) * Math.sqrt(lowersecond);
            let updown = upper / lower;
            if(!isNaN(updown)) {
                articleSimilarity.push([this.articlenames[i], updown]);
            }else{
                articleSimilarity.push([this.articlenames[i], 0])
            }
        }
        let sortedArray = articleSimilarity.sort(function(a, b) {
            return b[1] - a[1];
        });
        let finale = [];
        for(let x = 0; x<10; x++){
            finale.push(sortedArray[x]);
        }
        return finale;
    }

    distance(tree, pointx, pointy, dist, pointxfound, pointyfound){
        let point = false;
        if(pointx == pointy){
            return [true, true, dist];
        }
        if(tree.name == pointx && pointxfound != true){
            pointxfound = true;
            point = true;
        }
        if(tree.name == pointy && pointyfound != true){
            pointyfound = true;
            point = true;
        }
        if((pointxfound == true) && (pointyfound == true)){
            return [pointxfound, pointyfound, dist];
        }
        if('children' in tree) {
            for (let i = 0; i < tree.children.length; i++) {
                let res = this.distance(tree.children[i], pointx, pointy, dist+1, pointxfound, pointyfound);
                if(res[0] == true && res[1] == true && point == true){
                    return [res[0], res[1], res[2] - dist];
                }
                if (res[0] == true && res[1] == true) {
                    return [res[0], res[1], res[2]];
                }
                if(res[0] == true && pointxfound == false){
                    pointxfound = res[0];
                    dist = res[2];
                }
                if(res[1] == true && pointyfound == false){
                    pointyfound = res[1];
                    dist = res[2];
                }
            }
        }
        return [pointxfound, pointyfound, dist]
    }

    semantic(){
        let queries = this.selectedItems;
        let N = this.allFreq['entity'];
        let res = [];
        for(let article in this.articletypeid){
            let h=0;
            for(let q = 0; q<queries.length; q++){
                let max =[];
                for(let d = 0; d<this.articletypeid[article].length; d++){
                    let maxsim = [];
                    let sim = this.getCommonAncestors(queries[q], this.articletypeid[article][d]);
                    for(let p = 0; p<sim.length; p++){
                        let s = this.allFreq[sim[p].name];
                        s = s / N;
                        maxsim.push(-Math.log(s));
                    }
                    max.push(Math.max(...maxsim));
                }
                h += Math.max(...max);
            }
            res.push([article, h/queries.length]);
        }
        let sortedArray = res.sort(function(a, b) {
            return b[1] - a[1];
        });
        let finale = [];
        for(let x = 0; x<10; x++){
            finale.push(sortedArray[x]);
        }
        return finale;
    }

    pc(tree, N){
        return (this.frequenceOfParentChild(tree)/N);
    }

    frequenceOfParentChild(tree){
        let count = 0;
        for(let article in this.articletypeid){
            for(let i = 0; i<this.articletypeid[article].length; i++){
                if(this.articletypeid[article][i] == tree.name){
                    count +=1;
                }
            }
        }
        if('children' in tree){
            for(let j = 0; j<tree.children.length; j++){
                count += this.frequenceOfParentChild(tree.children[j]);
            }
        }
        this.allFreq[tree.name] = count;
        return count;
    }

    getCommonAncestors(pointx, pointy){
        let xancestors = this.getAncestors(this.treeData, pointx)[0];
        let yancestors = this.getAncestors(this.treeData, pointy)[0];
        let common =[];
        for(let i = 0; i<xancestors.length; i++){
            for(let j = 0; j<yancestors.length; j++){
                if(xancestors[i].name == yancestors[j].name){
                    common.push(xancestors[i]);
                }
            }
        }
        return common;
    }

    getAncestors(tree, point){
        if(point == tree.name){
            return  [[], true]
        }
        if('children' in tree){
            for (let i = 0; i < tree.children.length; i++) {
                let res = this.getAncestors(tree.children[i], point);
                if(res[1] == true){
                    let t = res[0];
                    t.push(tree);
                    return [t, true]
                }
            }
        }
        return [[], false];
    }

    pathlength(){
        let alldocs = this.articletypeid;
        let queries = this.selectedItems;
        let res = [];
        for(let article in alldocs){
            let h = 0;
            for(let q = 0; q<queries.length; q++){
                let max = [];
                for(let d = 0; d<alldocs[article].length; d++){
                    let dist = this.distance(this.treeData, queries[q], alldocs[article][d], 0, false, false)[2];
                    dist += 1;
                    max.push(1/dist);
                }
                h += Math.max(...max);
            }
            res.push([article, h/queries.length]);
        }
        let sortedArray = res.sort(function(a, b) {
            return b[1] - a[1];
        });
        let finale = [];
        for(let x = 0; x<10; x++){
            finale.push(sortedArray[x]);
        }
        return finale;
    }

    setTreeData(treedata){
        let data = JSON.parse(treedata);
        this.treeData = {name : 'entity', parent : '', children : data};
    }
};