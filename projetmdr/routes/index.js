var express = require('express');
const searchEngine = require('./../model/searchEngine')
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Search Engine', nom: ""});
});

router.post('/', function(req, res, next) {
  let result;
  let name;
  if("cosine" in req.body){
    name = "Cosine";
    delete req.body.cosine;
    delete req.body.treeData;
    let s = new searchEngine(req.body);
    result = s.cosineComputing();
  }
  if("pathlength" in req.body){
    name = "Path Length";
    delete req.body.pathlength;
    let treeData = req.body.treeData;
    delete req.body.treeData;
    let s = new searchEngine(req.body);
    s.setTreeData(treeData);
    result = s.pathlength();
  }
  if("semantic" in req.body){
    name = "Semantic";
    delete req.body.semantic;
    let treeData = req.body.treeData;
    delete req.body.treeData;
    let s = new searchEngine(req.body);
    s.setTreeData(treeData);
    s.frequenceOfParentChild(s.treeData);
    result = s.semantic();
  }
  res.render('index', { title: 'Search Engine', searchResult: result, nom: name });
});

module.exports = router;
