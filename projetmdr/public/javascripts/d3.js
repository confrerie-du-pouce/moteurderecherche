function unHide(evt) {
    let name = evt.target.getAttribute('name');
    let elements = document.getElementsByClassName(name);
    for(let i = 0; i<elements.length; i++){
        el = elements[i];
        if(el.hidden === true){
            el.hidden = false;
        }else{
            el.hidden = true;
        }
    }
}

function updateform2(tree, name, upper, parentName){
    let treeData;
    if(name === 'entity'){
        let treeForForm = document.createElement("input");
        treeForForm.setAttribute('name', 'treeData');
        treeForForm.setAttribute('value', JSON.stringify(tree));
        treeForForm.style.display = "none";
        upper.appendChild(treeForForm);
        treeData = {name : 'entity', parent: '', children: tree};
    }else{
        treeData = tree;
    }
    let ulist = document.createElement('ul');
    let lilist = document.createElement('li');
    let input = document.createElement('input');
    let label = document.createElement('label');

    input.setAttribute('type', 'checkbox');
    input.setAttribute('name', treeData.name);
    input.setAttribute('class', parentName);

    label.setAttribute('for', treeData.name);
    label.setAttribute('name', treeData.name);
    label.setAttribute('class', parentName);
    label.addEventListener("click", unHide, true);
    label.innerHTML = name;

    lilist.setAttribute('class', parentName);
    lilist.appendChild(input);
    lilist.appendChild(label);

    if('children' in treeData){
        for(let i = 0; i<treeData['children'].length; i++){
            updateform2(treeData['children'][i], treeData['children'][i]['name'], lilist, treeData.name);
        }
    }
    if(name != 'entity'){
        input.hidden = true;
        label.hidden = true;
        ulist.hidden = true;
        lilist.hidden = true;
    }
    ulist.setAttribute('class', parentName);
    ulist.appendChild(lilist);
    upper.appendChild(ulist);
}

d3.csv("/entities/ontology.csv").then(function(data) {
    let dataMap = data.reduce(function(map, node) {
        map[node.name] = node;
        return map;
    }, {});
    let treeData = [];
    data.forEach(function(node) {
        // add to parent
        let parent = dataMap[node.parent];
        if (parent) {
            // create child array if it doesn't exist
            (parent.children || (parent.children = []))
                // add node to child array
                .push(node);
        } else {
            // parent is null or missing
            treeData.push(node);
        }
    });
    updateform2(treeData, 'entity', document.getElementById('form'), 'entityParent');
});

